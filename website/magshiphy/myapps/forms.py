from django import forms

class SongForm(forms.Form):
    songname = forms.CharField(max_length=150)
    artist = forms.CharField(max_length=150)

class SearchForm(forms.Form):
    search = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Search...'}))