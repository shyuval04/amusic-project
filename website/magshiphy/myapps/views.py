from django.shortcuts import render
from myapps import forms
from django.contrib.auth.decorators import login_required
import requests
import json
import time
import base64

GET_URL="http://127.0.0.1:8000/api2/songs/?st=%s,%s"
GET_HISTORY_URL = "http://127.0.0.1:8000/api2/gethistory/?st=%s"
GET_RECOMM_URL = "http://127.0.0.1:8000/api2/getreccom/?st=%s"

# Create your views here.
def index(request):

    if request.user.is_authenticated and request.user.is_active:
        history = requests.get(GET_HISTORY_URL%(request.user.username))
        
        reccom =  requests.get(GET_RECOMM_URL%(request.user.username))
        
        piclist = ['01','02','03','04','05','06','07','08','09','10','11']

        if history:
            history = eval(history.text)
            print(history)
            addurl(history)
        else:
            history = []
        if reccom:
            reccom = eval(reccom.text)
            print(reccom)
            addurl(reccom)
        else:
            reccom = []

        print("History:", history)
        print("Recommended:", reccom)
        

        return render(request, 'new_test/index.html', { 'history' : history, 'reccom' : reccom , 'piclist': piclist, 'user_stat': True})
    else:
        return render(request, 'new_test/index.html', { 'history' : [], 'reccom' : [] , 'piclist': [], 'user_stat': False})
    


@login_required
def view_form(request):
    #form = forms.SongForm()
    form = forms.SearchForm()
    status = ''
    
    info = []
    if request.method == 'POST': #if submit is pressed
        #form = forms.SongForm(request.POST)
        form = forms.SearchForm(request.POST)

        if form.is_valid():
            #r = requests.get(GET_URL%(form.cleaned_data['songname'] ,form.cleaned_data['artist']))
            
            info = []
            msg = form.cleaned_data['search']
            text = ""
            for i in range(1,10):
                for sub in combed(msg):
                        r = requests.get(GET_URL%(sub[0] , sub[1] ))
                        print(sub)
                        if len(r.text) > 20: #not null
                            #for obj in json.loads(r.text):
                            #    print(obj['fields'])
                            #    info.append(obj['fields'])

                            text = "[%s]" % "},".join(r.text.split('}'))

                            print(text)
                            for obj in eval(text):
                                print(obj)
                                info.append(obj)
                            


           #smol little line to remove duplications
            info = [dict(t) for t in {tuple(d.items()) for d in info}]

            addurl(info)

            #print(info)
            #print(type(info))


            status= True
        else:
            status= False

    #getting history and reccomended

    history = requests.get(GET_HISTORY_URL%(request.user.username))
    
    reccom =  requests.get(GET_RECOMM_URL%(request.user.username))
    
    piclist = ['01','02','03','04','05','06','07','08','09','10','11']

    if history:
        history = eval(history.text)
        print(history)
        addurl(history)
    else:
        history = []
        
    if reccom:
        reccom = eval(reccom.text)
        print(reccom)
        addurl(reccom)
    else:
        reccom = []

    print("History:", history)
    print("Recommended:", reccom)
        
    return render(request, 'new_test/form.html',{'form' : form , 'status' : status, 'results': info ,'history' : history, 'reccom' : reccom , 'piclist': piclist }) #request , url , info passes in


#returns all combs of " " splitted string
def combed(big):

    songlist = big.split(" ")
    endlist = []
    
    for i in range(len(songlist)):
        endlist.append([ ' '.join(songlist[i:]) , ' '.join(songlist[:i])])
        endlist.append([' '.join(songlist[:i]),' '.join(songlist[i:])])
        print(endlist)

    return endlist

"""
gets the info stream (song stream)
returns a url shape of it
returns a list of urls 
"""
def addurl(strem):
    url = ''
    for litem in strem:
        
        url = "%s-%s" % ( litem['name'].replace(" ","+") , litem['author'].replace(" ","+")) 
        litem['url'] =  "/musicpage/?q=" + str(base64.b64encode(url.encode()), 'utf-8')
        #print( "#musicpage/?q=%s-%s" % ( litem['name'].replace(" ","+") , litem['author'].replace(" ","+") ) )
