from musicpage import getsong as AudGetter
from django.shortcuts import render
from shutil import copyfile
import urllib.request
import urllib.parse
import re
import os
import base64
import requests
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required

ADD_HISTORY_URL="http://127.0.0.1:8000/api2/addhistory/?st=%s,%s,%s"
GET_HISTORY_URL = "http://127.0.0.1:8000/api2/gethistory/?st=%s"
GET_RECOMM_URL = "http://127.0.0.1:8000/api2/getreccom/?st=%s"

REM_HISTORY_URL = "http://127.0.0.1:8000/api2/remhistory/?st=%s,%s,%s"


disliked = False


def getsong(searq):
    query_string = urllib.parse.urlencode({"search_query" : searq})
    html_content = urllib.request.urlopen("http://www.youtube.com/results?" + query_string)
    search_results = re.findall(r'href=\"\/watch\?v=(.{11})', html_content.read().decode())
    return search_results[0]


@login_required
def viewSong(request):
    
    if request.is_ajax():
        is_liked = request.GET.get('liked', None)

        print("im here")

        encstr = str(request.GET.get('q', ''))

        searchq = [i.split('+') for i in str(base64.b64decode(encstr).decode()).split('-')]
        
        #print(searchq)

        #print(" ".join(searchq[0])) #song name

        global disliked

        if is_liked == 'false':
            r = requests.get(REM_HISTORY_URL%( request.user.username , " ".join(searchq[0]) , " ".join(searchq[1]) ))
            disliked = True
        elif is_liked == 'true':
            r = requests.get(ADD_HISTORY_URL%( request.user.username , " ".join(searchq[0]) , " ".join(searchq[1]) ))
            print(r.text)
            disliked = False
        elif is_liked == 'lol not related' and disliked is False:
            r = requests.get(ADD_HISTORY_URL%( request.user.username , " ".join(searchq[0]) , " ".join(searchq[1]) ))
            print(r.text)

        ending = getsong(searchq)

        #yturl = "http://www.youtube.com/embed/" + ending

        if not os.path.isfile( os.path.join( os.path.join(os.path.join(os.environ['USERPROFILE']), 'Music',"Amusic") , "%s-%s.mp3"%(" ".join(searchq[0]) , " ".join(searchq[1]))) ):
            AudGetter.getAudioFromYoutubeVid('http://www.youtube.com/watch?v='+  ending)
            copyAudioFile(" ".join(searchq[0]), " ".join(searchq[1]))
        
        filename = os.path.join( os.path.join(os.path.join(os.environ['USERPROFILE']), 'Music',"Amusic") , "%s-%s.mp3"%(" ".join(searchq[0]) , " ".join(searchq[1])))


        history = requests.get(GET_HISTORY_URL%(request.user.username))
        
        reccom =  requests.get(GET_RECOMM_URL%(request.user.username))
        
        piclist = ['01','02','03','04','05','06','07','08','09','10','11']

        if history:
            history = eval(history.text)
            print(history)
            addurl(history)
        else:
            history = []
        if reccom:
            reccom = eval(reccom.text)
            print(reccom)
            addurl(reccom)
        else:
            reccom = []

            
        html_str = render_to_string("new_test/musicpage.html",{'filename': filename, 'name': " ".join(searchq[0]), 'author' : " ".join(searchq[1]) , 'history' : history, 'reccom' : reccom , 'piclist': piclist})
            
        #input(html_str)

        return JsonResponse({ "page_content" : html_str })

    else:
        return render(request,"new_test/loading.html",{'elurl': "%s?q=%s" % (request.path , request.GET.get('q', '')) })
    """
    encstr = str(request.GET.get('q', ''))

    searchq = [i.split('+') for i in str(base64.b64decode(encstr).decode()).split('-')]
    
    #print(searchq)

    #print(" ".join(searchq[0])) #song name

    r = requests.get(ADD_HISTORY_URL%( 'Dummy' , " ".join(searchq[0]) , " ".join(searchq[1]) ))

    print(r.text)

    ending = getsong(searchq)

    #yturl = "http://www.youtube.com/embed/" + ending

    if not os.path.isfile( os.path.join( os.path.join(os.path.join(os.environ['USERPROFILE']), 'Music',"Amusic") , "%s-%s.mp3"%(" ".join(searchq[0]) , " ".join(searchq[1]))) ):
        AudGetter.getAudioFromYoutubeVid('http://www.youtube.com/watch?v='+  ending)
        copyAudioFile(" ".join(searchq[0]), " ".join(searchq[1]))
    
    filename = os.path.join( os.path.join(os.path.join(os.environ['USERPROFILE']), 'Music',"Amusic") , "%s-%s.mp3"%(" ".join(searchq[0]) , " ".join(searchq[1])))


    history = requests.get(GET_HISTORY_URL%("Dummy"))
    
    reccom =  requests.get(GET_RECOMM_URL%("Dummy"))
    
    piclist = ['01','02','03','04','05','06','07','08','09','10','11']

    if history:
        history = eval(history.text)
        print(history)
        addurl(history)
    else:
        history = []
    if reccom:
        reccom = eval(reccom.text)
        print(reccom)
        addurl(reccom)
    else:
        reccom = []

    return render(request,"new_test/musicpage.html",{'filename': filename, 'name': " ".join(searchq[0]), 'author' : " ".join(searchq[1]) , 'history' : history, 'reccom' : reccom , 'piclist': piclist})
    """





    

def copyAudioFile(name, author):

    src_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    allfiles = os.listdir(src_dir) # Lists all files in the current directory
    for item in allfiles: # iterate over all files in the current directory
        if item.endswith('.mp3'): # Checks if the file is a mp3 file
            #os.system('/usr/local/bin/essentia_streaming_extractor_music '+item+' '+item+'.json')

            mus_dir = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Music',"Amusic")
            
            if not os.path.isdir(mus_dir):
                os.mkdir(mus_dir)

            os.replace(item,os.path.join(mus_dir , "%s-%s.mp3"%(name , author)))

            break

"""
gets the info stream (song stream)
returns a url shape of it
returns a list of urls 
"""
def addurl(strem):
    url = ''
    for litem in strem:
        
        url = "%s-%s" % ( litem['name'].replace(" ","+") , litem['author'].replace(" ","+")) 
        litem['url'] =  "/musicpage/?q=" + str(base64.b64encode(url.encode()), 'utf-8')
        #print( "#musicpage/?q=%s-%s" % ( litem['name'].replace(" ","+") , litem['author'].replace(" ","+") ) )


