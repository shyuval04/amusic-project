from . import views
from django.urls import path , include

urlpatterns = [
    path('users/', views.users, name="view_users"),
    path('songs/', views.songs, name="view_songs"),
    path('getreccom/', views.get_reccomendations, name="get_reccomendations"), #website
    path('addreccom/', views.add_reccomendations, name="add_reccomendations"), #ai
    path('addhistory/', views.add_history, name="add_history"),
    path('gethistory/', views.get_history, name="get_history"),
    path('remhistory/', views.rem_history, name="rem_history"),


    
    
    #path(r'^songs/(?P<songname>\w{0,50})/$', views.songs,)
]