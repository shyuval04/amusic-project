from django.shortcuts import render
from django.http import JsonResponse,HttpResponse
from myAPI import models
from django.core import serializers
from myAPI.DatasetWrapper import song_api
from django.contrib.auth.models import User

# Create your views here.


def users(request):
    if request.method == "GET":

        param_stre =  request.GET.get('st', '').split(',')

        print(param_stre)
        
        #extract songname and artist
        try:secretword = param_stre[0] if param_stre[0] else ''
        except: secretword = ''


        #display api filtered
        if secretword == 'this_is_an_unthinkable_sentance':
            users = str([str(user) for user in User.objects.all()])
        else:
            users = "access denied"
        return HttpResponse(users)

"""
gets songs by search query
"""
def songs(request):
    #songs = models.Song()
    if request.method == "GET":

        #getting param stream
        param_stre =  request.GET.get('st', '').split(',')
        
        #extract songname and artist
        try:songname = param_stre[0] if param_stre[0] else ''
        except: songname = ''
        try:artistname = param_stre[1] if param_stre[1] else ''
        except:artistname=''

        #display api filtered
        if songname is '' and artistname is '':
            #serialized_obj = serializers.serialize('json', models.Song.objects.all())
            serialized_obj = song_api.get_parsed_songs(r"SELECT * FROM SONGS LIMIT 20;")
        else:
            if songname is not '' and artistname is '':
                serialized_obj = song_api.get_parsed_songs("SELECT * FROM SONGS WHERE song_name LIKE '%%%s%%';" % (param_stre[0] ) )

            elif songname is '' and artistname is not'':
                serialized_obj = song_api.get_parsed_songs("SELECT * FROM SONGS WHERE  artist_name LIKE '%%%s%%';" % (param_stre[1]))

            else:
                serialized_obj = song_api.get_parsed_songs("SELECT * FROM SONGS WHERE song_name LIKE '%%%s%%' AND artist_name LIKE '%%%s%%';" % (param_stre[0] ,param_stre[1] ) )

            print(serialized_obj)

        return HttpResponse(serialized_obj)

"""
get reccomendations to user
"""
def get_reccomendations(request):
    
    if request.method == "GET":
        #getting param stream
        param_stre =  request.GET.get('st', '').split(',')
        print(param_stre)
        #extract user id,  songname and artist
        try:username = param_stre[0] if param_stre[0] else ''
        except:username=''

        print(username)
        
        #display api filtered
        if username is '':
            serialized_obj = "No / not enough params were passed..."
        else:
            
            song_id = song_api.get_songs_id_from_history("SELECT * FROM SONGS_RECCOMENDATIONS WHERE user_id = '%s' ORDER BY Timestamp DESC LIMIT 10;" % (username))

            print(song_id)

            songlist = [] 
            for d in song_id:
                songlist.append(song_api.get_parsed_songs("SELECT * FROM SONGS WHERE song_id = %s;" % (d['song_id']))[0] ) 
            
            serialized_obj = str(songlist)

            #print(serialized_obj)

        return HttpResponse(serialized_obj)

"""
add reccomendations to reccomlist
"""
def add_reccomendations(request):
    
    if request.method == "GET":
        param_stre =  request.GET.get('st', '').split(',')
        
        #extract user id,  songname and artist
        try:username = param_stre[0] if param_stre[0] else ''
        except:username=''
        try:songname = param_stre[1] if param_stre[1] else ''
        except: songname = ''
        try:artistname = param_stre[2] if param_stre[1] else ''
        except:artistname=''

        print(username)
        print(songname)
        print(artistname)

        #display api filtered
        if  username is '' or songname is '' or artistname is '' :
            serialized_obj = "No / not enough params were passed..."
        else:

            song_id = song_api.get_songs_id("SELECT * FROM SONGS WHERE song_name LIKE '%%%s%%' AND artist_name LIKE '%%%s%%';" % (songname ,artistname ))[0]

            if (song_api.insert_songs_to_rec(username , song_id) ):
                print("yeyeyeyeyeyeyeyey")

            print(song_id)

            serialized_obj = str(song_id)

            #print(serialized_obj)

        return HttpResponse(serialized_obj)

"""
get user's song history
params: user
"""
def get_history(request):
    
    if request.method == "GET":
        #getting param stream
        param_stre =  request.GET.get('st', '').split(',')
        print(param_stre)
        #extract user id,  songname and artist
        try:username = param_stre[0] if param_stre[0] else ''
        except:username=''

        print(username)
        
        #display api filtered
        if username is '':
            serialized_obj = "No / not enough params were passed..."
        else:
            
            song_id = song_api.get_songs_id_from_history("SELECT * FROM SONGS_HISTORY WHERE user_id = '%s' ORDER BY Timestamp DESC LIMIT 10;" % (username))

            print(song_id)

            songlist = [] 
            for d in song_id:
                songlist.append(song_api.get_parsed_songs("SELECT * FROM SONGS WHERE song_id = %s;" % (d['song_id']))[0] ) 
            
            serialized_obj = str(songlist)

            #input(serialized_obj)

        return HttpResponse(serialized_obj)


"""
adds song to users history
params: user, song, artist
"""
def add_history(request):
    
    if request.method == "GET":
        #getting param stream
        param_stre =  request.GET.get('st', '').split(',')
        
        #extract user id,  songname and artist
        try:username = param_stre[0] if param_stre[0] else ''
        except:username=''
        try:songname = param_stre[1] if param_stre[1] else ''
        except: songname = ''
        try:artistname = param_stre[2] if param_stre[1] else ''
        except:artistname=''

        print(username)
        print(songname)
        print(artistname)

        #display api filtered
        if  username is '' or songname is '' or artistname is '' :
            serialized_obj = "No / not enough params were passed..."
        else:
            
            song_id = song_api.get_songs_id("SELECT * FROM SONGS WHERE song_name LIKE '%%%s%%' AND artist_name LIKE '%%%s%%';" % (songname ,artistname ))[0]

            if (song_api.insert_songs(username , song_id) ):
                print("yeyeyeyeyeyeyeyey")

            print(song_id)

            serialized_obj = str(song_id)

            #print(serialized_obj)

        return HttpResponse(serialized_obj)


"""
removes song from users history
params: user, song, artist
"""
def rem_history(request):
    
    if request.method == "GET":
        #getting param stream
        param_stre =  request.GET.get('st', '').split(',')
        
        #extract user id,  songname and artist
        try:username = param_stre[0] if param_stre[0] else ''
        except:username=''
        try:songname = param_stre[1] if param_stre[1] else ''
        except: songname = ''
        try:artistname = param_stre[2] if param_stre[1] else ''
        except:artistname=''

        print(username)
        print(songname)
        print(artistname)

        #display api filtered
        if  username is '' or songname is '' or artistname is '' :
            serialized_obj = "No / not enough params were passed..."
        else:
            
            song_id = song_api.get_songs_id("SELECT * FROM SONGS WHERE song_name LIKE '%%%s%%' AND artist_name LIKE '%%%s%%';" % (songname ,artistname ) )[0]


            if (song_api.remove_songs(username , song_id) ):
                print("yeyeyeyeyeyeyeyey")

            print(song_id)

            serialized_obj = str(song_id)

            #print(serialized_obj)

        return HttpResponse(serialized_obj)

    