"""magshiphy URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/dev/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from myAPI import urls
from myapps import views
from register import views as regV
from musicpage import views as musicV
from django.contrib import admin
from django.urls import path , re_path , include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [
    path('', views.index, name='index'),
    path('form/', views.view_form, name='form_name'),
    path('api2/', include(urls)),
    path('register/', regV.register, name='register'),
    path('login/', regV.logintotheweb, name='loginto'),
    path('logout/', regV.logoutoftheweb, name='logoutof'),
    path('musicpage/', musicV.viewSong, name='musicpage'),
    path('admin/', admin.site.urls),
    re_path(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        regV.activate, name='activate'),
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)