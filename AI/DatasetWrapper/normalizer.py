import sqlite3
from sqlite3 import Error
import numpy as np
import pandas as pd
from sklearn import preprocessing

#function normalizes the song database, by removing uneeded dimentions, patching holes (putting values in NAN places) and created a normalized version of the database
def normalize_db(conn):
    query = "SELECT * FROM SONGS;"#select all songs
    song_df = pd.read_sql_query(query, conn)#get the entrie dataframe

    if np.count_nonzero(song_df.isnull().sum()) > 0:#if there is missing data
        song_df.fillna(song_df.median(), inplace = True)#fill in the missing holes with the median of the missing values' column
        song_df.to_sql('SONGS', con=conn, if_exists='replace', index=False)#change the table to the new standart

    #drop empty columns
    index_of_empty_columns = song_df.nunique().isin([0,1])#get the index of empty columns
    empty_columns = np.array(song_df.columns)[index_of_empty_columns]#get column names

    useless_columns = np.array(['song_id', 'song_name', 'song_name_normalized', 'artist_name', 'artist_name_normalized'])#those columns contain useless information like ID or strings (and using One-Hot takes too much proceccing power)
    
    columns_to_drop = np.concatenate((useless_columns, empty_columns), axis=None)
    song_df.drop(columns=columns_to_drop, inplace=True)#drom columns

    #get normalized values
    values = song_df.values #returns a numpy array
    min_max_scaler = preprocessing.MinMaxScaler()
    values_scaled = min_max_scaler.fit_transform(values)
    norm_df = pd.DataFrame(values_scaled, columns=song_df.columns)

    norm_df.to_sql('NORM_SONGS', con=conn, if_exists='replace', index_label = 'song_id')

    #just checking if it went alright
    query = "SELECT * FROM NORM_SONGS;"#select all normalized songs
    norm_df = pd.read_sql_query(query, conn)
    print(norm_df.head())