import sqlite3
from sqlite3 import Error
import numpy as np
import pandas as pd
from sklearn.neighbors import NearestNeighbors
import requests
import time

def create_model(songs, n_songs):
    songs = songs.drop(['song_id'], axis=1)
    recommender = NearestNeighbors(n_neighbors=n_songs, algorithm='ball_tree')
    nbrs = recommender.fit(songs)
    
    return nbrs

#function creates mean songs for the user
#input: the songs of the user (pd dataframe), the paremeter weights (np array)
#output: the mean songs of the user (pd dataframe), which are points in the k-dim array that represent the 'perfect' song for the user
def get_mean_song(user_songs, song_df):
    user_songs_df = pd.DataFrame(columns=song_df.columns)
    #input(song_df)
    #input(user_songs_df)

    for song in user_songs:
        user_songs_df = user_songs_df.append(song_df.iloc[song['id'] + 1])#the +1 is because the SONG_ID starts from 1 and i dont have time rn to patch it

    return pd.DataFrame(user_songs_df.mean()).transpose()#if there is a problem in the Knn shape try to remove the transpose

#function gets the recommended songs for the user
#input: all normalized songs (pd dataset), songs that the user heard (pd dataset), parameter weights (np array)
#output: the recommended songs for the user (pd dataframe)
def get_recommended_songs(user_songs, song_df):
    song_df = song_df.drop(['song_id'], axis=1)

    mean_song = get_mean_song(user_songs, song_df)
    #input(mean_song)
    
    distances, indices = nbrs.kneighbors(mean_song)

    recommended_norm_songs = pd.DataFrame()

    for index_list in indices:
        for index in index_list:
            if index + 1 not in user_songs:
                recommended_norm_songs = recommended_norm_songs.append(song_df.iloc[index])

    return recommended_norm_songs

def connect_to_dataset(dataset_path):
    conn = None
    try:#try to connect to the sqlite3 file
        conn = sqlite3.connect(dataset_path)
    except Error as e:
        print("SQL ERROR:")
        print(e)

    return conn

def get_users():
    URL = "http://127.0.0.1:8000/api2/users/?st=this_is_an_unthinkable_sentance"
    r = requests.get(url = URL)
    data = eval(r.text)
    return data

def get_user_history(user):
    URL = "http://127.0.0.1:8000/api2/gethistory/?st=" + user
    r = requests.get(url = URL)
    data = eval(r.text)#the individual values may or may not still be STR
    return data


if __name__ == '__main__':
    conn = connect_to_dataset(r"Subset.db")

    query = "SELECT * FROM NORM_SONGS;"#select all songs
    norm_songs_df = pd.read_sql_query(query, conn)#get the entrie dataframe

    query = "SELECT * FROM SONGS;"#select all songs
    songs_df = pd.read_sql_query(query, conn)#get the entrie dataframe

    n_recommended_songs = 10
    nbrs = create_model(norm_songs_df , n_recommended_songs)

    users = get_users()
    recommendation_df = pd.DataFrame(columns=["user", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",])#if this line fails, it means that the users variable is not a list for some reason
    for user in users:
        recommendation_df.append(pd.DataFrame({"user":[user], "1":[0], "2":[0], "3":[0], "4":[0], "5":[0], "6":[0], "7":[0], "8":[0], "9":[0], "10":[0]}))

    while True:
        for user in users:
            history = get_user_history(user)

            if(len(history) < 2):
                continue

            rec_songs = get_recommended_songs(history, norm_songs_df).transpose().to_dict()

            user_row = recommendation_df.loc[recommendation_df['user'] == user]

            for song in rec_songs:#go over all the recommended songs for the user to check if there are any new recommendations
                
                if song not in user_row:
                    URL = "http://127.0.0.1:8000/api2/addreccom/?st=" + user + "," + songs_df.iloc[song]["song_name"] + "," + songs_df.iloc[song]["artist_name"]#update the recommendation table
                    requests.get(url = URL)
            
            #i know there is probebly a more elegant way of updating the recommendation dataframe, but im on a tight scedual
            i = 1
            for song in rec_songs:
                recommendation_df.at[user, str(i)] = song
                i = i + 1
        time.sleep(300)#wait for 5 minutes untill the next check

