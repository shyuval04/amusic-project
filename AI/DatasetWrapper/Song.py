import hdf5_getters as getters
import os
import re

# non alphanumeric
re_nonalphanum = re.compile(r'\W')

def remove_nonalphanumeric(s):
    """
    Remove usual punctuation signs:  ! , ? : ; . '   etc
    Also, we transform long spaces into normal ones
    """
    # split around non-alphanum chars
    parts = re_nonalphanum.split(s)
    # remove empty spots
    parts = filter(lambda p: p, parts)
    # rejoin with regular space ' '
    return ' '.join(parts)

#this class holds all the data we need for song representation, both with the model and with the website
class SongData:
        def __init__(self, h5, songidx):
            self.song_name = str(getters.get_title(h5, songidx))[2:-1]#string
            self.song_name_normalized = remove_nonalphanumeric(self.song_name).lower()#string. normalized becaused non-alphanumeric characters mess with SQL queries
            self.artist_name = str(getters.get_artist_name(h5, songidx))[2:-1]#string
            self.artist_name_normalized = remove_nonalphanumeric(self.artist_name).lower()#string
            self.artist_familiarity = getters.get_artist_familiarity(h5, songidx)#float
            self.danceability = getters.get_danceability(h5, songidx)#float
            self.duration = getters.get_duration(h5, songidx)#float
            self.energy = getters.get_energy(h5, songidx)#float
            self.key = getters.get_key(h5, songidx)#int
            self.key_confidence = getters.get_key_confidence(h5, songidx)#float
            self.loudness = getters.get_loudness(h5, songidx)#float
            self.mode = getters.get_mode(h5, songidx)#int
            self.mode_confidence = getters.get_mode_confidence(h5, songidx)#float
            self.tempo = getters.get_tempo(h5, songidx)#float
            self.time_signature = getters.get_time_signature(h5, songidx)#int
            self.time_signature_confidence = getters.get_time_signature_confidence(h5, songidx)#int

        