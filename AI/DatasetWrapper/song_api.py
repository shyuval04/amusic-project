import os.path
import sqlite3
from sqlite3 import Error
import numpy as np
import pandas as pd
from sklearn.neighbors import NearestNeighbors

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
db_path = os.path.join(BASE_DIR , r"Subset.db")



"""

creating song history table

CREATE TABLE `SONGS_HISTORY` (
	`user_id`	TEXT NOT NULL,
	`song_id`	INTEGER NOT NULL,
	`Timestamp`	DATETIME DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(`user_id`,`song_id`)
);

insert history:
INSERT INTO `SONGS_HISTORY` VALUES( (text)user id , song id , CURRENT_TIMESTAMP)

insert, and upadte if exists:

INSERT OR IGNORE INTO SONGS_HISTORY (user_id,song_id) VALUES( 'asdasd' , 1 );
  
UPDATE SONGS_HISTORY SET Timestamp=current_timestamp WHERE user_id='asdasd' AND song_id=1;

"""

#Website related functions:

def get_parsed_songs(query):
    
    try:#try to connect to the sqlite3 file
        conn = sqlite3.connect(db_path)
    except Error as e:
        print("SQL ERROR:")
        print(e)

    songs = pd.read_sql_query(query, conn)
    
    parsed_songs = songs[['song_name', 'artist_name']]
    
    return parsed_songs

def get_songs_id(query):

    try:#try to connect to the sqlite3 file
        conn = sqlite3.connect(db_path)
    except Error as e:
        print("SQL ERROR:")
        print(e)

    songs = pd.read_sql_query(query, conn)

    parsed_songs = songs['somg_id']
    return parsed_songs

def get_songs_id_from_history(query):

    try:#try to connect to the sqlite3 file
        conn = sqlite3.connect(db_path)
    except Error as e:
        print("SQL ERROR:")
        print(e)

    songs = pd.read_sql_query(query, conn)

    parsed_songs = songs['song_id']
    
    return parsed_songs

def insert_songs(user_id, song_id):

    try:#try to connect to the sqlite3 file
        conn = sqlite3.connect(db_path)
    except Error as e:
        print("SQL ERROR:")
        print(e)
        return False

    
    data = (user_id, int(song_id['song_id']))

    cur = conn.cursor()
    cur.execute("INSERT OR IGNORE INTO SONGS_HISTORY (user_id,song_id) VALUES( ? , ? );", data)
    cur.execute("UPDATE SONGS_HISTORY SET Timestamp=current_timestamp WHERE user_id=? AND song_id=?;", data)
    conn.commit()
    conn.close()
    return True   

def insert_songs_to_rec(user_id, song_id):

    try:#try to connect to the sqlite3 file
        conn = sqlite3.connect(db_path)
    except Error as e:
        print("SQL ERROR:")
        print(e)
        return False

    
    data = (user_id, int(song_id['song_id']))

    cur = conn.cursor()
    cur.execute("INSERT OR IGNORE INTO SONGS_RECCOMENDATIONS (user_id,song_id) VALUES( ? , ? );", data)
    cur.execute("UPDATE SONGS_RECCOMENDATIONS SET Timestamp=current_timestamp WHERE user_id=? AND song_id=?;", data)
    conn.commit()
    conn.close()
    return True  